class CreateVisits < ActiveRecord::Migration[5.2]
  def change
    create_table :visits do |t|
      t.date :date
      t.string :description
      
      t.references :person, foreign_key: true
      t.references :pet, foreign_key: true

      t.timestamps
    end
  end
end
