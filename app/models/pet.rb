class Pet < ApplicationRecord
  belongs_to :person
  has_many :visits
end
