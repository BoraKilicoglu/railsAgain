class Visit < ApplicationRecord
  belongs_to :person
  belongs_to :pet
end
