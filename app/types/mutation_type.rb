MutationType = GraphQL::ObjectType.define do
  name "Mutation"
  description "The mutation root for this schema"

  field :createPerson, PersonType do
    description "Create a person"
    argument :person, PersonInputType

    resolve ->(obj, args, ctx) {
      Person.create(args[:person].to_h)
    }
  end

  field :editPerson, PersonType do
    description "Edit person"
    argument :person, PersonInputType

    resolve ->(obj, args, ctx) {
      person = Person.find(args[:person][:personId])
      person.update(name: args[:person][:name])
      person.update(surname: args[:person][:surname])
      person
    }
  end

  field :createVisit, VisitType do
    description "Create a visit"
    argument :visit, VisitInputType

    resolve ->(obj, args, ctx) {
      Visit.create(args[:visit].to_h)
    }
  end

  field :createPet, PetType do
    description "Create a pet"
    argument :pet, PetInputType

    resolve ->(obj, args, ctx) {
      Pet.create(args[:pet].to_h)
    }
  end

  field :editPet, PetType do
    description "Edit person"
    argument :pet, PetInputType
  
    resolve ->(obj, args, ctx) {
      pet = Pet.find(args[:pet][:personId])
      pet.update(name: args[:pet][:name])
      pet.update(person_id: args[:pet][:person_id])
      pet
    }
  end
end

PersonInputType = GraphQL::InputObjectType.define do
  name "personInputType"
  description "Properties for creating a person"

  argument :personId, types.Int do
    description "ID of the person."
  end

  argument :name, !types.String do
    description "Name of the person."
  end

  argument :surname, !types.String do
    description "Name of the surname."
  end
end

PetInputType = GraphQL::InputObjectType.define do
  name "petInputType"
  description "Properties for creating a pet"

  argument :person_id, types.Int do
    description "Owner of the pet."
  end

  argument :name, !types.String do
    description "Name of the pet."
  end
end

VisitInputType = GraphQL::InputObjectType.define do
  name "visitInputType"
  description "Properties for creating a visit"

  argument :person_id, types.Int do
    description "Owner of the visit."
  end
  
  argument :pet_id, types.Int do
    description "Pet of the visit."
  end

  argument :description, !types.String do
    description "Description of the visit."
  end
end