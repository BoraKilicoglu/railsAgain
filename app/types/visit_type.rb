VisitType = GraphQL::ObjectType.define do
  name "Visit"
  description "A Visit"
  field :id, types.ID
  field :date, types.String
  field :description, types.String
  field :person do type PersonType
    resolve -> (visit, args, ctx) { visit.person }
  end
  field :pet do type PetType
    resolve -> (visit, args, ctx) { visit.pet }
  end
end